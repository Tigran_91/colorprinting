let $ = jQuery;
let barsContainer = document.querySelector(".bars");
let bars = document.getElementsByClassName(".bars__item");
let navigation = document.querySelector(".mainNavigation");
barsContainer.addEventListener("click", () => {
  let substring = new RegExp("bars--open");
  if (substring.test(barsContainer.classList.value)) {
    navigation.classList.remove("mainNavigation--open");
    document.body.classList.remove("scroll-lock");
    // document.getElementsByTagName("html").classList.remove("scroll-lock");
    barsContainer.classList.remove("bars--open");
  } else {
    navigation.classList.add("mainNavigation--open");
    document.body.classList.add("scroll-lock");
    // document.getElementsByTagName("html").classList.add("scroll-lock");
    barsContainer.classList.add("bars--open");
  }
});

// Loader
let hideLoader = function() {
  //   setTimeout(() => {
  //     document.querySelector(".loader").classList.add("loader--hidden");
  //   }, 2000);
  $(".loader").fadeOut("slow");
};

// Login
let login = document.getElementsByClassName("authentication__login")[0];
let loginModal = document.getElementsByClassName("loginModal")[0];
let loginModalWrapper = document.getElementsByClassName(
  "loginModal__wrapper"
)[0];
login.addEventListener("click", function() {
  loginModal.classList.add("loginModal--opened");
});
document.addEventListener("click", function(el) {
  if (el.target.className === "loginModal__wrapper") {
    loginModal.classList.remove("loginModal--opened");
  }
});
document.addEventListener("keyup", function(el) {
  if (el.keyCode  === 27) {
    loginModal.classList.remove("loginModal--opened");
  }
});


// Check each input focusin/focusout
function onFormFocus() {
  this.classList.add("inputField__title--filled");
}
function onFormBlur() {
  if (!this.value) {
    this.classList.remove("inputField__title--filled");
  }
}
let forms = document.getElementsByClassName("inputField__item");
for (const formIndex in forms) {
  if (forms.hasOwnProperty(formIndex)) {
    const form = forms[formIndex];
    if (form.addEventListener) {
      // focus/blur на стадии перехвата срабатывают во всех браузерах
      // поэтому используем их
      form.addEventListener("focus", onFormFocus, true);
      form.addEventListener("blur", onFormBlur, true);
    } else {
      // ветка для IE8-, где нет стадии перехвата, но есть focusin/focusout
      form.onfocusin = onFormFocus;
      form.onfocusout = onFormBlur;
    }
  }
}

// David
// tabbed content
// http://www.entheosweb.com/tutorials/css/tabs.asp
jQuery(document).ready(function() {
  let tabFunction = function() {
    $(".tab_content").hide();
    $(".tab_content:first").show();

    /* if in tab mode */
    $("ul.tabs li").click(function() {
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel");
      $("#" + activeTab).fadeIn();

      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

      $(".tab_drawer_heading").removeClass("d_active");
      $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");
    });
    /* if in drawer mode */
    $(".tab_drawer_heading").click(function() {
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel");
      $("#" + d_activeTab).fadeIn();

      $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");

      $("ul.tabs li").removeClass("active");
      $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
    });

    /* Extra class "tab_last" 
        to add border to right side
        of last tab */
    $("ul.tabs li")
      .last()
      .addClass("tab_last");
  };
  if (
    document.body.className.indexOf("recentPostsPage") !== -1 ||
    document.body.className.indexOf("productDetailsPage") !== -1 ||
    document.body.className.indexOf("checkoutPage") !== -1
  ) {
    tabFunction();
  }

  // Testimonies Slider
  if (document.body.className.indexOf("homePage") !== -1) {
    $(".testimonial__slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      dotsClass: "testimonial__dots fx"
    });
  }

  // Related Products Slider
  if (document.body.className.indexOf("productDetailsPage") !== -1) {
    $(".ourProduct__wrapper").slick({
      slidesToShow: 2,
      slidesToScroll: 2,
      nextArrow:
        '<button type="button" class="slick-next ourProduct__arrow ourProduct__arrowNext"><i class="fa fa-chevron-right ourProduct__icon" aria-hidden="true"></i></button>',
      prevArrow:
        '<button type="button" class="slick-prev ourProduct__arrow ourProduct__arrowPrev"><i class="fa fa-chevron-left ourProduct__icon" aria-hidden="true"></i></button>',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {}
        }
      ]
    });
  }
});

let isMinWidth = window.matchMedia("(min-width: 768px)").matches;

const productsMasonry = () => {
  if (window.matchMedia("(min-width: 768px)").matches) {
    const testimoniesMasonry = new Masonry(".testimoniesWrapper", {
      gutter: 30
    });
  }
};

if (document.body.className.indexOf("productsPage") !== -1) {
  // Loader
  setTimeout(() => hideLoader(), 2000);

  // Masonry
  productsMasonry();
  window.addEventListener("resize", () => {
    if (window.matchMedia("(min-width: 768px)").matches) {
      if (!isMinWidth) {
        productsMasonry();
        isMinWidth = true;
        console.log("If: ");
      }
    } else if (isMinWidth) {
      console.log("else: ");
      isMinWidth = false;
    }
  });
}

//  Input[type=number] code

(function($) {
  "use strict";

  function InputNumber(element) {
    this.$el = $(element);
    this.$input = this.$el.find("[type=text]");
    this.$inc = this.$el.find("[data-increment]");
    this.$dec = this.$el.find("[data-decrement]");
    this.min = this.$el.attr("min") || false;
    this.max = this.$el.attr("max") || false;
    this.init();
  }

  InputNumber.prototype = {
    init: function() {
      this.$dec.on("click", $.proxy(this.decrement, this));
      this.$inc.on("click", $.proxy(this.increment, this));
    },

    increment: function(e) {
      var value = this.$input[0].value;
      value++;
      console.log(value, this.max);
      if (!this.max || value <= this.max) {
        this.$input[0].value = value++;
      }
    },

    decrement: function(e) {
      var value = this.$input[0].value;
      value--;
      if (!this.min || value >= this.min) {
        this.$input[0].value = value;
      }
    }
  };

  $.fn.inputNumber = function(option) {
    return this.each(function() {
      var $this = $(this),
        data = $this.data("inputNumber");

      if (!data) {
        $this.data("inputNumber", (data = new InputNumber(this)));
      }
    });
  };

  $.fn.inputNumber.Constructor = InputNumber;
})(jQuery);

$(".input-number").inputNumber();
//  Input[type=number] code end

$(document).ready(function() {
  $(".btn-close").on("click", function() {
    $(this)
      .parents("tr")
      .slideUp(600);
  });
});
